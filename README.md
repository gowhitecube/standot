# Standot

Standot is an old-fashioned chat bot that collects and records daily
standups of Scrum teams that are fully remote.

The bot is designed for [Matrix.org chat protocol](https://matrix.org/blog/home/).

## Quick Start

```bash
$ cd standot
$ bundle
$ cp config_template.yml config.yml
$ edit config.yml
$ ruby app.rb
```

## Use with Docker

This is the recommended way to get the app up and running quickly.
Multiple instances can be configured separately, and logs can be easily
accessed with `docker logs`.

Docker can be installed with `sudo snap install docker` in Ubuntu.

1. Build a Docker image from the source.

```bash
$ cd standot
$ docker build -t standot .
```

2. Create a new directory per team to manage data and configs
   separately.

```bash
$ mkdir team_alpha && cd team_alpha
$ mkdir db
$ cp ~/standot/config_template.yml config.yml
```
3. Edit configuration.

```bash
$ vim config.yml
```

3. Launch a Docker container from within the team directory with
   specified configuration. Do not forget to specify a custom port
   (`$YOUR_PORT`).

   Data persists to the `database.db` file stored in `db/` path in the
   local file system.

```bash
$ docker run -p $YOUR_PORT:4567 -v $PWD/config.yml:/standot/config.yml -v $PWD/db:/standot/db standot
```

## Bare Installation on a Fresh Ubuntu (Bionic) VPS

After launching the VPS and logging in via SSH as root, follow the
instructions to get the VPS ready for standot.

1. Perform updates and install dependencies.

```bash
$ apt-get update && apt-get upgrade -y
$ apt-get install -y ruby git curl sudo tmux build-essential libsqlite3-dev libssl1.0-dev zlib1g-dev libreadline-dev autoconf bison build-essential libyaml-dev libreadline-dev libncurses5-dev libffi-dev libgdbm-dev
```

2. Add and configure a user, `standot`. Provide a new password for the
   user when prompted.

```bash
$ adduser standot
$ usermod -aG sudo standot
```

3. Exit as root and log-in again as `standot`

```bash
$ exit
```

```bash
$ ssh standot@yourserver
```

4. Install `rbenv` Ruby version manager 

```bash
$ curl -sL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash -
$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
$ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
$ source ~/.bashrc
```

5. Build and install `Ruby 2.3.7` via rbenv. This step might take a while.

```bash
$ rbenv install 2.3.7
```

6. Clone the repository, configure Ruby version, install Gem
   dependencies, configure app, and run.

```bash
$ git clone $REPOSITORY_URL
$ cd standot
$ rbenv local 2.3.7
$ gem install bundler
$ bundle
$ cp config_template.yml config.yml
$ edit config.yml
$ ruby app.rb
```

`tmux` can be used to persist running instances of the app across SSH
logins. Create a new tmux window with `tmux new`, launch the app and
detach. The window can be restored with `tmux -2 a` upon following
logins.

