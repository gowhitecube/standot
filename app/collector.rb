class StandupCollector
  def initialize(user)
    @user      = user
    @quit_time = DateTime.parse(Config.quit_time)
    @client    = Matrix::Client.new(Config.server_uri)
  end

  def invite
    response = @client.invite(Config.report_room, @user.user_id)

    if response.status != 200
      $LOG.error "The user \"#{@user.display_name}\" cannot be invited to the common standup room: '#{response.body["errcode"]}: #{response.body["error"]}'"
      return false
    end

    $LOG.info "The user \"#{@user.display_name}\" has been invited to the common standup room."
    true
  end

  def setup
    auth = @client.login(Config.bot_username, Config.bot_password)

    if auth.status != 200
      $LOG.error "The bot, \"#{Config.bot_username}\" cannot login: '#{auth.body["errcode"]}: #{auth.body["error"]}'"
      return false
    end

    unless display_name && room_id
      $LOG.error "The user #{@user.user_id} cannot be validated."
      return false
    end

    if @client.join(room_id).status != 200
      $LOG.error "The bot cannot join room \"#{room_id}\": '#{auth.body["errcode"]}: #{auth.body["error"]}'"
      return false
    end

    true
  end

  def get_standup_answers; answers = []

    $LOG.info "Asking standup questions to user \"#{@user.display_name}.\""

    Config.questions.each do |q|
      answer = gather_answer(q)

      if answer
        @user.standups.create(
          question: q,
          answer: answer
        )

        answers << answer
      end
    end

    answers.count == Config.questions.count && answers.select { |answer| answer.empty? }.empty? ? success : failure
  end

  def gather_answer(question)
    @client.send(question)

    response = []; waited = 0
    while ((response.empty? || waited < Config.timeout) && DateTime.now < @quit_time) do
      sleep Config.pause; waited += Config.pause

      if r = @client.messages_from(sender_id: @user.user_id)
        response << r
        waited = 0
      end

      response.flatten!
    end

    if response.empty?
      return false
    end

    $LOG.info "Got an answer from user \"#{@user.display_name}.\""

    response.map do |r|
      r["content"]["body"]
    end.join("\n")
  end

  def success
    @client.send(Config.done)

    @client.join(Config.report_room)

    template = ERB.new <<-EOF
      **<%= @user.display_name %>** has successfully participated in daily standup. Here's the report.
      <% @user.last_standup.each do |answer| %>
          "<%= answer[:question] %>"
          <%= answer[:answer].gsub(/^/, '> ') %>
      <% end %>
      ----
    EOF

    @client.send(template.result(binding).gsub(/^[ ]+/, ''))

    $LOG.info "\"#{@user.display_name}\" has successfully participated in daily standup."
  end

  def failure
    @client.send(Config.not_done)

    r = @client.join(Config.report_room)

    template = ERB.new <<-EOF
      **<%= @user.display_name %>** has not participated in daily standup.
      ----
    EOF

    @client.send(template.result(binding).gsub(/^[ ]+/, ''))

    $LOG.info "\"#{@user.display_name}\" has *not* participated in daily standup."
  end

  def room_id
    @user.update(room_id: @client.create_pm(@user.user_id).body["room_id"]) if @user.room_id.empty?
    @user.room_id
  end

  def display_name
    @user.update(display_name: @client.get_display_name(@user.user_id).body["displayname"]) if @user.display_name.empty?
    @user.display_name
  end
end
